package id.co.ptdmc.modabile;

import id.co.ptdmc.modabile.handlers.ContentHandler;
import id.co.ptdmc.modabile.handlers.HeaderContentHandler;
import id.co.ptdmc.modabile.handlers.HeaderHandler;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.util.Headers;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {
		HeaderContentHandler handler = new HeaderContentHandler(Headers.CONTENT_TYPE_STRING, "text/plain", "Hello Modabile");
		ContentHandler contentHandler = new ContentHandler("Hello Modabile", null);
		HeaderHandler headerHandler = new HeaderHandler(Headers.CONTENT_TYPE_STRING, "text/plain", contentHandler);

		ServletEngine engine = ServletEngine.getInstance();
		PathHandler pathHandler = engine.getPathHandler();

		Undertow server = Undertow.builder()
		        .addHttpListener(83, "localhost")
		        .setHandler(pathHandler)
		        .build();
		server.start();
	}

}
