package id.co.ptdmc.modabile.handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class HeaderHandler implements HttpHandler {

	private HttpString header;
	private String value;
	private HttpHandler next;

	public HeaderHandler(String header, String value, HttpHandler next) {
		super();
		this.header = new HttpString(header);
		this.value = value;
		this.next = next;
	}

	public void handleRequest(HttpServerExchange exchange) throws Exception {
		exchange.getResponseHeaders().put(header, value);
		if (next != null) {
			next.handleRequest(exchange);
		}
	}

}
