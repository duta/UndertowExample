package id.co.ptdmc.modabile.handlers;

import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;

import java.io.IOException;

public class WebSocketHandler {

	private HttpHandler wsHandler;
	private HttpHandler resourceHandler;

	public WebSocketHandler() {
		WebSocketConnectionCallback socketCallback = new WebSocketConnectionCallback() {
			public void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {
				channel.getReceiveSetter().set(new AbstractReceiveListener() {

					@Override
					protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) throws IOException {
						WebSockets.sendText(message.getData(), channel, null);
					}
					
				});
				channel.resumeReceives();
			}
		};

		wsHandler = Handlers.websocket(socketCallback);
		resourceHandler = Handlers.resource(new ClassPathResourceManager(
								WebSocketHandler.class.getClassLoader(),
								WebSocketHandler.class.getPackage())).addWelcomeFiles("index.html");
	}

	/**
	 * @return the wsHandler
	 */
	public HttpHandler getWsHandler() {
		return wsHandler;
	}

	/**
	 * @return the resourceHandler
	 */
	public HttpHandler getResourceHandler() {
		return resourceHandler;
	}

}
