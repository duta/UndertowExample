package id.co.ptdmc.modabile;

import id.co.ptdmc.modabile.handlers.WebSocketHandler;
import id.co.ptdmc.modabile.servlets.HelloServlet;
import id.co.ptdmc.modabile.servlets.IndexServlet;
import id.co.ptdmc.modabile.servlets.MyServlet;
import io.undertow.Handlers;
import io.undertow.jsp.HackInstanceManager;
import io.undertow.jsp.JspServletBuilder;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletContainer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.jasper.deploy.JspPropertyGroup;
import org.apache.jasper.deploy.TagLibraryInfo;

public class ServletEngine {

	private static final ServletEngine INSTANCE = new ServletEngine();

	private PathHandler pathHandler;

	private ServletEngine() {
		try {
			initEngine();
		} catch (ServletException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void initEngine() throws ServletException, IOException {
		// servlet
		DeploymentInfo servletBuilder = Servlets.deployment()
				.setClassLoader(ServletEngine.class.getClassLoader())
				.setContextPath("/Modabile")
				.setDeploymentName("modabile.war")
				.addServlets(
						Servlets.servlet("IndexServlet", IndexServlet.class)
				                .addMapping("/"),
						Servlets.servlet("HelloServlet", HelloServlet.class)
						        .addMapping("/hello"),
						Servlets.servlet("MyServlet", MyServlet.class)
						        .addMapping("/my")
						);

		// websocket
		WebSocketHandler socketHandler = new WebSocketHandler();

		// JSP
		DeploymentInfo jspBuilder = Servlets.deployment()
				.setClassLoader(ServletEngine.class.getClassLoader())
				.setContextPath("/jsp")
				.setDeploymentName("modabile.war")
				.setResourceManager(new DefaultResourceLoader(ServletEngine.class))
				.addServlet(JspServletBuilder.createServlet("Default Jsp Servlet", "*.jsp"));

		Map<String, TagLibraryInfo> tagLibInfo = TldLocator.createTldInfos();
	    JspServletBuilder.setupDeployment(jspBuilder, new HashMap<String, JspPropertyGroup>(), tagLibInfo, new HackInstanceManager());

	    // Create handler
	    ServletContainer container = Servlets.defaultContainer();
	    DeploymentManager managerServlet = container.addDeployment(servletBuilder);
	    managerServlet.deploy();
	    
	    DeploymentManager managerJsp = container.addDeployment(jspBuilder);
	    managerJsp.deploy();

		pathHandler = Handlers.path(Handlers.redirect("/Modabile"))
				.addPrefixPath(servletBuilder.getContextPath(), managerServlet.start())
				.addPrefixPath("ModabileSocket", socketHandler.getWsHandler())
				.addPrefixPath("/SocketClient", socketHandler.getResourceHandler())
				.addPrefixPath(jspBuilder.getContextPath(), managerJsp.start());
	}

	public static ServletEngine getInstance() {
		return INSTANCE;
	}

	public PathHandler getPathHandler() {
		return pathHandler;
	}

	public static class DefaultResourceLoader extends ClassPathResourceManager {
		public DefaultResourceLoader(final Class<?> clazz) {
			super(clazz.getClassLoader(), "");
		}
	}

}
